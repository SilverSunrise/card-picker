package sho.mony.cardpicker

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import sho.mony.cardpicker.R.drawable.*

class MainActivity : AppCompatActivity() {

    private lateinit var imageCard: List<ImageView>
    private var score: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val images: MutableList<Int> = data()
        game(images)
    }

    @SuppressLint("SetTextI18n")
    private fun data(): MutableList<Int> {
        val images: MutableList<Int> =
            mutableListOf(
                card_2,
                card_3,
                card_4,
                card_5,
                card_6,
                card_7,
                card_8,
                card_9,
                card_10,
                card_j,
                card_q,
                card_k,
                card_a
            )
        imageCard = listOf(
            btn_card_1,
            btn_card_2,
            btn_card_3,
            btn_card_4,
            iv_randomCard
        )
        tv_score.text = "Score: $score"
        return images
    }

    private fun game(images: MutableList<Int>) {
        var rnd1 = (0..12).random()
        var rnd2 = (0..12).random()
        var rnd3 = (0..12).random()
        var rnd4 = (0..12).random()
        val rnd5 = (0..9).random()

        if ((rnd1 == rnd2) or (rnd1 == rnd3) or (rnd1 == rnd4)) {
            rnd1 = (0..12).random()
        }
        if ((rnd2 == rnd3) or (rnd2 == rnd4) or (rnd2 == rnd1)) {
            rnd2 = (0..12).random()
        }
        if ((rnd3 == rnd4) or (rnd3 == rnd1) or (rnd3 == rnd2)) {
            rnd3 = (0..12).random()
        }
        if ((rnd4 == rnd1) or (rnd4 == rnd2) or (rnd4 == rnd3)) {
            rnd4 = (0..12).random()
        }

        btn_card_1.setImageResource(images[rnd1])
        btn_card_2.setImageResource(images[rnd2])
        btn_card_3.setImageResource(images[rnd3])
        btn_card_4.setImageResource(images[rnd4])
        iv_randomCard.setImageResource(images[rnd5])

        btn_card_1.setOnClickListener {
            if (rnd1 > rnd5) {
                win(images)
            } else {
                lose(images)
            }
        }
        btn_card_2.setOnClickListener {
            if (rnd2 > rnd5) {
                win(images)
            } else {
                lose(images)
            }
        }
        btn_card_3.setOnClickListener {
            if (rnd3 > rnd5) {
                win(images)
            } else {
                lose(images)
            }
        }
        btn_card_4.setOnClickListener {
            if (rnd4 > rnd5) {
                win(images)
            } else {
                lose(images)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun win(images: MutableList<Int>) {
        score += 1
        tv_score.text = "Score: $score"
        game(images)
    }

    @SuppressLint("SetTextI18n")
    private fun lose(images: MutableList<Int>) {
        if (score > 0) {
            score -= 1
        }
        tv_score.text = "Score: $score"
        game(images)
    }
}